package org.drivemybox.interview.service;

import org.springframework.stereotype.Component;

@Component
public interface GreetingService {

   String greet(String name);
}
