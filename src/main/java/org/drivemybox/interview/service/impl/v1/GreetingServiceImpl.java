package org.drivemybox.interview.service.impl.v1;

import org.drivemybox.interview.config.ConfigProperties;
import org.drivemybox.interview.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("GreetingServiceV1")
public class GreetingServiceImpl implements GreetingService {

   @Autowired
   private ConfigProperties configProperties;

   public GreetingServiceImpl() {
   }

   // wrapping the value into getter for easier testing
   String getGreeting() {
      return configProperties.getGreeting();
   }

   @Override
   public String greet(String name) {
      return String.format("%s, %s", getGreeting(), name);
   }
}
