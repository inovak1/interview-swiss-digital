package org.drivemybox.interview.service.impl.v2;

import org.drivemybox.interview.service.GreetingService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Component("GreetingServiceV2")
public class GreetingServiceImpl implements GreetingService {

   public GreetingServiceImpl() {
   }

   @Override
   public String greet(String name) {
      var currentDateTime = getCurrentDateTime();
      var currentTime = LocalTime.of(currentDateTime.getHour(), currentDateTime.getMinute(), currentDateTime.getSecond(), currentDateTime.getNano());


      String greeting;
      if (getCondition(currentTime, getTimeToCompareWith(2, 59), getTimeToCompareWith(11, 59))) {
         greeting = "Good morning";
      } else if (getCondition(currentTime, getTimeToCompareWith(11, 59), getTimeToCompareWith(16, 59))) {
         greeting = "Good afternoon";
      } else if (getCondition(currentTime, getTimeToCompareWith(16, 59), getTimeToCompareWith(19, 59))) {
         greeting = "Good evening";
      } else {
         greeting = "Good night";
      }

      return String.format("%s, %s", greeting, name);
   }

   private boolean getCondition(LocalTime currentTime, LocalTime beforeTime, LocalTime afterTime) {
      return (currentTime.compareTo(beforeTime) == 0 || currentTime.isAfter(beforeTime)) && currentTime.isBefore(afterTime);
   }

   private LocalTime getTimeToCompareWith(int hours, int minutes) {
      return LocalTime.of(hours, minutes, 0, 0);
   }

   // wrapping into a getter for easier testing
   LocalDateTime getCurrentDateTime() {
      return LocalDateTime.now();
   }
}
