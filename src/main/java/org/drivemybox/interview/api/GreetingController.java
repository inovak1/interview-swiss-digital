package org.drivemybox.interview.api;

import org.drivemybox.interview.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

   private GreetingService greetingService;

   @Autowired
   public GreetingController(@Qualifier("greetingService") GreetingService greetingService) {
      this.greetingService = greetingService;
   }

   @GetMapping("/")
   public String defaultMapping(@RequestParam(value = "name", defaultValue = "unknown person") String name) {
      return greetingService.greet(name);
   }

   @GetMapping("/greeting")
   public String greet(@RequestParam(value = "name", defaultValue = "unknown person") String name) {
      return greetingService.greet(name);
   }
}
