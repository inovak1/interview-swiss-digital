package org.drivemybox.interview.config;

import org.drivemybox.interview.api.Greeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class ConfigProperties {

   private String greeting;

   @Autowired
   public ConfigProperties(@Value("${interview.app.greet}") String greeting) {
      this.greeting = Greeting.valueOf(greeting).getGreeting();
   }

   /**
    * Getting the value from the ConfigProperties to prevent the app from starting if property is not provided.
    * @return If a known value is set to the property it will be fetch the value from the org.drivemybox.interview.api.Greeting enum.
    */
   public String getGreeting() {
      return greeting;
   }
}
