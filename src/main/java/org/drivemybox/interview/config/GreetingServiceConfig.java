package org.drivemybox.interview.config;

import org.drivemybox.interview.service.GreetingService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class GreetingServiceConfig {

   @Bean("greetingService")
   @ConditionalOnProperty(
         value = "interview.app.service.greeting.version",
         havingValue = "1.0",
         matchIfMissing = true)
   /**
    *
    */
   GreetingService greetingServiceV1() {
      return new org.drivemybox.interview.service.impl.v1.GreetingServiceImpl();
   }

   @Bean("greetingService")
   @ConditionalOnProperty(
         value = "interview.app.service.greeting.version",
         havingValue = "2.0")
   GreetingService greetingServiceV2() {
      return new org.drivemybox.interview.service.impl.v2.GreetingServiceImpl();
   }

}
