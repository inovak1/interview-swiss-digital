package org.drivemybox.interview.service.impl.v1;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

class GreetingServiceImplTest {

   @Test
   void greetShouldReturnExpectedValue() {
      // given
      var greetingService = spy(new GreetingServiceImpl());
      doReturn("Hellooo").when(greetingService).getGreeting();

      // when
      var result = greetingService.greet("Random");

      // then
      assertThat(result).isEqualTo("Hellooo, Random");
   }
}