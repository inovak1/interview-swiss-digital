package org.drivemybox.interview.service.impl.v2;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

class GreetingServiceImplTest {

   @ParameterizedTest
   @MethodSource("timeAndGreetingArguments")
   void greetShouldGreetWithProperResponseForTheTimeOfDay(LocalDateTime givenDateTime, String greeting) {
      // given
      var greetingService = spy(new GreetingServiceImpl());
      doReturn(givenDateTime).when(greetingService).getCurrentDateTime();

      // when
      var result = greetingService.greet("Random");

      // then
      assertThat(result).isEqualTo("%s, Random", greeting);
   }

   private static Stream<Arguments> timeAndGreetingArguments() {
      return Stream.of(
            // normal cases
            Arguments.of(LocalDateTime.of(2021, 5, 18, 2, 59, 0, 000000001), "Good morning"),
            Arguments.of(LocalDateTime.of(2021, 5, 18, 11, 58, 59, 999999999), "Good morning"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  11, 59, 0, 000000001), "Good afternoon"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  16, 58, 59, 999999999), "Good afternoon"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  16, 59, 0, 000000001), "Good evening"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  19, 58, 59, 999999999), "Good evening"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  19, 59, 0, 000000001), "Good night"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  2, 58, 59, 999999999), "Good night"),

            // edge cases
            Arguments.of(LocalDateTime.of(2021, 5, 18, 2, 59, 0, 0), "Good morning"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  11, 59, 0, 0), "Good afternoon"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  16, 59, 0, 0), "Good evening"),
            Arguments.of(LocalDateTime.of(2021, 5, 18,  19, 59, 0, 0), "Good night")
      );
   }

}