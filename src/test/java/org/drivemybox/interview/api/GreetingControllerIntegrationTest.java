package org.drivemybox.interview.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.drivemybox.interview.InterviewApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.util.UriComponentsBuilder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


@SpringBootTest(
      classes = InterviewApplication.class,
      webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class GreetingControllerIntegrationTest {

   private HttpClient httpClient;

   private String basePath;

   @LocalServerPort
   private int port;

   @BeforeEach
   public void setup() {
      basePath = "http://localhost:" + port;
      httpClient = HttpClient.newHttpClient();
   }

   @Test
   @DisplayName("'/' endpoint should return the default greeting if no param provided")
   public void defaultMappingShouldReturnDefaultGreeting() throws Exception {
      // given
      var uri = UriComponentsBuilder.fromHttpUrl(basePath)
            .build()
            .toUri();

      var httpRequest = HttpRequest.newBuilder()
            .uri(uri)
            .GET()
            .build();

      // when
      var response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

      // then
      assertEquals(200, response.statusCode());
      assertEquals("Hi, unknown person", response.body());
   }

   @Test
   @DisplayName("'/' endpoint return the correct greeting if param provided")
   public void defaultMappingShouldReturnTheCorrectGreeting() throws Exception {
      // given
      var uri = UriComponentsBuilder.fromHttpUrl(basePath)
            .queryParam("name", "Leonard")
            .build()
            .toUri();

      var httpRequest = HttpRequest.newBuilder()
            .uri(uri)
            .GET()
            .build();

      // when
      var response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

      // then
      assertEquals(200, response.statusCode());
      assertEquals("Hi, Leonard", response.body());
   }

   @Test
   @DisplayName("'/greet' endpoint should return the default greeting if no param provided")
   public void greetShouldReturnDefaultGreeting() throws Exception {
      // given
      var uri = UriComponentsBuilder.fromHttpUrl(basePath + "/greeting")
            .build()
            .toUri();

      var httpRequest = HttpRequest.newBuilder()
            .uri(uri)
            .GET()
            .build();

      // when
      var response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

      // then
      assertEquals(200, response.statusCode());
      assertEquals("Hi, unknown person", response.body());
   }

   @Test
   @DisplayName("'/greet' endpoint return the correct greeting if param provided")
   public void greetShouldReturnTheCorrectGreeting() throws Exception {
      // given
      var uri = UriComponentsBuilder.fromHttpUrl(basePath + "/greeting")
            .queryParam("name", "Leonard")
            .build()
            .toUri();

      var httpRequest = HttpRequest.newBuilder()
            .uri(uri)
            .GET()
            .build();

      // when
      var response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

      // then
      assertEquals(200, response.statusCode());
      assertEquals("Hi, Leonard", response.body());
   }

}