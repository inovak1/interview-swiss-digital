# Interview app

A basic [Spring Boot](http://projects.spring.io/spring-boot/) app for the job interview.

## Requirements

For building and running the application you need:

- [JDK 1.11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Maven 3](https://maven.apache.org)
- [Docker](https://www.docker.com/get-started)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `org.drivemybox.interview.InterviewApplication` class from your IDE.

You can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

or, using the Maven Wrapper which is packaged with the application from you preferred CLI tool:

```shell
./mvnw spring-boot:run
```

## Building the Docker Image

The easiest way to build the Docker image:

```shell
docker build . -t interview
```

This will create:

* A build image called "build_image"
* The actual image with the app called "interview"

## Running the Docker Image

```shell
docker run --rm -p 8080:8080 interview
```

## Configuring the app for local use

The table below shows all the configurable properties of the app:

| Property                        | Accepted values | Required                                    |
| ------------------------------- | --------------- | ------------------------------------------- |
| spring.profiles.active          | dev, test, prod | Yes, app will not start                     |
| interview.app.greet             | HELLO, HI, HEY  | Yes, app will not start                     |
| interview.app.service.version   | 1.0, 2.0        | No, if not provided, 1.0 is used as default |

The default profile can be set in the `application.yml` file. Other properties can be defined in the environment specific yml file e.g. `application-dev.yml`.

## Configuring the app with Docker

All the properties can be overridden during the start of the docker image.

```shell
docker run --rm --env interview.app.greet='HEY' -p 8080:8080 interview
```