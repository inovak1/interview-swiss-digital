FROM maven:3.8.1-jdk-11 AS build_image
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:11
COPY --from=build_image /usr/src/app/target/interview-0.0.1-SNAPSHOT.jar /usr/app/interview-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/interview-0.0.1-SNAPSHOT.jar"]